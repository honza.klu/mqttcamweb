from setuptools import setup

setup(
    name='mqttcamweb',
    version='0.0.1',
    description='Show images from mqtt',
    author='Jan Klusacek',
    author_email='honza.klu@gmail.com',
    packages=['mqttcamweb'],
    include_package_data=True,
    python_requires='>=3.6',
    install_requires=[
        "paho-mqtt",
        "pyyml",
        "flask",
        "flask-socketio",
        "pyaml",
    ],
)
