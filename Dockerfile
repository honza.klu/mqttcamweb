FROM debian:latest

WORKDIR /root

ENTRYPOINT python3 -m mqttimgdumper --settings /etc/mqttimgdumper.yaml

RUN apt-get -y update
RUN apt-get -y install pip git
RUN pip install waitress git+https://gitlab.com/honza.klu/mqttimgdumper