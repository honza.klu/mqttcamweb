import time
import os
import re
import yaml
import logging
logger = logging.getLogger()

from flask import Flask
from flask_socketio import SocketIO, send, emit
import paho.mqtt.client as mqtt

class MqttHandler:
    def __init__(self, host, port, uname, passwd, socketio, topics):
        self.host = host
        self.port = port
        self.uname = uname
        self.passwd = passwd
        self.socketio = socketio
        self.topics = topics

        self.client = mqtt.Client(client_id="ASDF")
        self.client.loop_start()
        self.client.on_message = self.on_message
        self.client.on_disconnect = self.on_disconnect
        self.client.on_connect = self.on_connect

    def connect(self):
        print("Try to connect")
        self.client.username_pw_set(self.uname, self.passwd)
        self.client.connect(self.host, self.port, 60)

    def on_connect(self, client, userdata, flags, rc):
        print("CONNECTED")
        for topic in self.topics:
            client.subscribe(topic)
        #client.subscribe("/esp32cam_table/img")

    def on_message(self, client, userdata, msg):
        print(f"MESSAGE TOPIC {msg.topic}")
        self.socketio.emit('new_img', msg.payload)

    def on_disconnect(self, userdata, rc, properties):
        print("DISCONNECTED")
        self.connect()

def create_app(test_config=None):
    print("FACTORY")
    app = Flask(__name__)
    socketio = SocketIO(app)

    mqtt_sett = yaml.load(open(os.environ.get("CAMWEB_CONF_PATH"), "r"), yaml.SafeLoader) #"./example_settings.yaml"

    @app.route("/")
    def html_index():
        return """<!DOCTYPE html>
    <body>

    <img id="main_img"></img>
    </body>

    <script src = "https://www.tutorialspoint.com/jquery/jquery-3.6.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/4.0.1/socket.io.js" integrity="sha512-q/dWJ3kcmjBLU4Qc47E4A9kTB4m3wuTY7vkFJDTZKjTs8jhyGQnaUrxa0Ytd0ssMZhbNua9hE+E7Qv1j+DyZwA==" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf-8">
        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }
        var socket = io();
        socket.on('connect', function() {
            socket.emit('my event', {data: 'Im connected!'});
        });

        socket.on('new_img', function(msg) {
            console.log("Received img");
            var oldImg = document.getElementById('main_img');
            var newImg = new Image();        

            const blob = new Blob([msg], {type:"image/jpeg"} );
            oldImg.src = URL.createObjectURL(blob)
        });
    </script>

        """

    @socketio.on('message')
    def handle_message(data):
        print('received message: ' + data)

    @socketio.on('my event')
    def handle_my_custom_event(json):
        print('received event json: ' + str(json))

    con = mqtt_sett["connections"][0]
    app.mqtt_handler = MqttHandler(con["host"], con["port"], con["user"], con["password"], socketio, ["/esp32cam_window/img"])
    app.mqtt_handler.connect()
    return app